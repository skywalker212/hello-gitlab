var prom = (name)=>{
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            try{
                resolve(`Hello ${name?name:'anon'}`);
            }catch(e){
                reject(e);
            }
        },5000);
    });
}

prom().then((data)=>{
    console.log(data);
}).catch((error)=>{
    console.error(error);
});